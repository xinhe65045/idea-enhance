# 项目介绍
```IDEA全家桶：使用姿势、插件、LiveTemplate等。(基于社区版：2018.2)```
![](.README_images/2a10de97.png)

# 常用插件
- generateO2O
- lombok
- intellij-spring-assistant
- CamelCasePlugin
- MavenRunHelper
- PasteImages
- TranslationPlugin

# 推荐配置

# 正确打开姿势
## 多行编辑
### 基本操作

### 使用正则

## LiveTemplate
### 内置Template

### 常用Template扩展

## 开发自己的IDEA插件